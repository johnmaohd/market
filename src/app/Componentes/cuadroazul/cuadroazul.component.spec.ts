import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuadroazulComponent } from './cuadroazul.component';

describe('CuadroazulComponent', () => {
  let component: CuadroazulComponent;
  let fixture: ComponentFixture<CuadroazulComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuadroazulComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CuadroazulComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
