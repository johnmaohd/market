import { Component } from '@angular/core';

@Component({
  selector: 'cuadrorojo',
  templateUrl: './cuadrorojo.component.html',
  styleUrls: ['./cuadrorojo.component.css']
})
export class CuadrorojoComponent {
  public letrero: string;
  public year: number;

  constructor() {
    this.letrero = "soy el letrero del componente rojo";
    this.year = 2023;

    console.log("componente rojo cargado", this.year, this.letrero)

  }

}


