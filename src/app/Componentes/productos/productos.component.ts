import { Component } from '@angular/core';
import { ApiService, Product } from './api.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent {
  products: Product[] = [];

  constructor(private apiService: ApiService) {
    this.getProducts();
  }

  getProducts(): void {
    this.apiService.getProducts().subscribe({
      next: (data: Product[]) => {
        this.products = data;
        console.log(this.products);
      },
      error: (error) => {
        console.error('Error al obtener productos:', error);
      }
    });
  }
}


