import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CuadroazulComponent } from './Componentes/cuadroazul/cuadroazul.component';
import {CuadrorojoComponent} from "./Componentes/cuadrorojo/cuadrorojo.component";
import { NavbarComponent } from './Componentes/navbar/navbar.component';
import { JumbotronComponent } from './Componentes/jumbotron/jumbotron.component';
import { CarouselComponent } from './Componentes/carousel/carousel.component';
import { FooterComponent } from './Componentes/footer/footer.component';
import { ProductosComponent } from './Componentes/productos/productos.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    CuadroazulComponent,
    CuadrorojoComponent,
    NavbarComponent,
    JumbotronComponent,
    CarouselComponent,
    FooterComponent,
    ProductosComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
